import React from "react";
import PropTypes from "prop-types";
import WebFont from "webfontloader";
import statuses from "./statuses";

const noop = () => {};

class WebfontLoader extends React.Component {
  state = {
    status: undefined
  };

  handleLoading = () => {
    this.setState({ status: statuses.loading });
  };

  handleActive = () => {
    this.setState({ status: statuses.active });
  };

  handleInactive = () => {
    this.setState({ status: statuses.inactive });
  };

  componentWillMount() {
    const { config } = this.props;
    WebFont.load({
      ...config,
      loading: this.handleLoading,
      active: this.handleActive,
      inactive: this.handleInactive
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { onStatus } = this.props;
    if (prevState.status !== this.state.status) {
      onStatus(this.state.status);
    }
  }

  render() {
    const { children } = this.props;
    return children || null;
  }
}

WebfontLoader.propTypes = {
  config: PropTypes.object.isRequired,
  children: PropTypes.element,
  onStatus: PropTypes.func.isRequired
};

WebfontLoader.defaultProps = {
  onStatus: noop
};

export default WebfontLoader;
