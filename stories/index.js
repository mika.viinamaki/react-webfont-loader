import React from "react";

import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";

import WebfontLoader from "../src/index";
import { injectGlobal } from "styled-components";

injectGlobal`

  body {
    font-family: 'Source Sans Pro';
  }
`;

// webfontloader configuration object.
const config = {
  google: {
    families: ["Source Sans Pro:300,600"]
  }
};

storiesOf("WebfontLoader", module)
  .add("Webfont from Google Fonts", () =>
    <WebfontLoader config={config} onStatus={action("status")}>
      <div>
        <h1>Hello world!</h1>

        <p>Lorem ipsum, dolor sit amet.</p>
      </div>
    </WebfontLoader>
  )
  .add("As a self-loading tag", () =>
    <div>
      <WebfontLoader config={config} onStatus={action("status")} />
      <h1>WebfontLoader can also be used without children</h1>
      <p>Lorem ipsum, dolor sit amet.</p>
    </div>
  );
